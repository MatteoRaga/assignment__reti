#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include <sys/wait.h>
#include<sys/socket.h>
#include<sys/time.h>
#include<netinet/in.h>
#include<signal.h>
#include<errno.h>
#include<unistd.h>

void handle_signal (int sig);
void initializeStructure();
void printStructure ();
void loadFdSet(fd_set* rSet);
int getMaxSfd();

