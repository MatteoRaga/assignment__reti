#include "superserver.h"

#define BACK_LOG 2 // Maximum number of request of clients
#define N_SERVICE 4 //number of different services

typedef struct service{
    char fullpth[15];
    char *name;
    char protocol[3];
    int port; 
    char type[8]; 
    int sfd; 
    int pid; 
}t_service;

t_service services[N_SERVICE];

int  main(int argc,char **argv,char **env){ // NOTE: env is the variable to be passed, as last argument, to execle system-call
    fd_set readSet;
    struct sockaddr_in server_addr[N_SERVICE];
    struct sockaddr_in client_addr[N_SERVICE]; 
    int i = 0;
    int connectFd;
    int pid;
    int nready;
    socklen_t cli_size;

	//reads from file superserver.conf and populate the structure
	initializeStructure();

	printStructure();

    //populate server_addr for each service
    for(i=0; i <N_SERVICE ; i++){
		server_addr[i].sin_family = AF_INET; 
		server_addr[i].sin_addr.s_addr = htonl(INADDR_ANY); 
		server_addr[i].sin_port = htons(services[i].port); 
		bind(services[i].sfd, (struct sockaddr*)&server_addr[i], sizeof(server_addr[i])); 
		if (services[i].protocol[0] == 't'){
			listen(services[i].sfd, BACK_LOG);  
		}
    }

	signal (SIGCHLD,handle_signal); // Handle signals sent by son processes 
    
    for(;;){
		//clear and reinsert all the sfd into readSet 
		loadFdSet(&readSet);
		nready = select(getMaxSfd()+1, &readSet, NULL, NULL, NULL); 
		if(nready<0){ //select fired for an error
			if(errno == 4){
				printf("select fired for a signal\n");
				fflush(stdout);
			}else{
			printf("select error\n");
			return 1;
			}
		}else{	
			for(i=0; i < N_SERVICE ; i++){
				if((FD_ISSET(services[i].sfd, &readSet))){
					printf("select fired for a request of %s %s %d\n",services[i].name, services[i].type, services[i].port);
					if (services[i].protocol[0] == 't'){			    
						cli_size = sizeof(client_addr);
						connectFd = accept(services[i].sfd,(struct sockaddr * )&client_addr, &cli_size);
					}
					pid = fork();
					//processo figlio
					if (pid == 0 ) { 
						fclose(stdout);
						fclose(stderr);
						fclose(stdin);
						if (services[i].protocol[0] == 't'){
							dup(connectFd);
							dup(connectFd);
							dup(connectFd);
							close(services[i].sfd);
						}else{
							dup(services[i].sfd);
							dup(services[i].sfd);
							dup(services[i].sfd);
						}
						execle(services[i].fullpth, services[i].name,NULL,env); 
					}else { 
						if (services[i].protocol[0] == 't'){
							close(connectFd);	
						}
						if(services[i].type[0] == 'w'){
							services[i].pid = pid;
							FD_CLR(services[i].sfd, &readSet);
						}
						
					}
				}
			}
		}
	}

	return 0;
}

void handle_signal (int sig){
	pid_t cpid;
    cpid = wait(NULL);
    switch (sig) {
        case SIGCHLD : 
				for(int i=0; i< N_SERVICE ; i++){
					if(services[i].pid == cpid){
						if(services[i].type[0] = 'w'){
							services[i].pid = 0;
						}
					}
				}
            	break;
        default : printf ("Signal not known!\n");
    			 break;
    }
}

void printStructure (){	
	for(int i=0;i<N_SERVICE;i++){	
		printf("----SERVICE %d----",i);
		printf("\n%s", services[i].fullpth);
		printf("\n%s", services[i].name);
		printf("\n%d", services[i].sfd);
		printf("\n%s", services[i].protocol);
		printf("\n%d", services[i].port);
		printf("\n%d", services[i].pid);	
		printf("\n%s", services[i].type);
		printf("\n");
	}
}

void initializeStructure(){
	FILE *file;
	fd_set readSet;
	char buf[200];
	char *res;
	int i=0;

	file=fopen("superserver.conf", "r");
	if( file==NULL ) {
	perror("Errore in opening the file");
		exit(1);
	}

    res=fgets(buf, 200, file);
    while(res!=NULL ) {
		sscanf(buf,"%s %s %d %s",services[i].fullpth,services[i].protocol,&services[i].port,services[i].type);
		i++;
    	res=fgets(buf, 200, file);
	}

    fclose(file);

	for(int i=0;i<N_SERVICE;i++){
		if(services[i].protocol[0] == 'u') {
			services[i].name = "udpServer.exe";
			services[i].sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		}else{
			services[i].name = "tcpServer.exe";
			services[i].sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		}
			services[i].pid = 0; //initially we put each service's pid to 0
	}
}

int getMaxSfd(){
	int maxSfd = 0;
	for(int i = 0 ;i<N_SERVICE; i++){
		if(services[i].sfd > maxSfd) maxSfd = services[i].sfd;
	}
	return maxSfd;
}

void loadFdSet(fd_set* rSet){
	FD_ZERO(rSet); 
	for(int i=0; i< N_SERVICE ; i++){
	    if(services[i].pid == 0){
		FD_SET(services[i].sfd, rSet);
	    }
	}
}	
