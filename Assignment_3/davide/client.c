#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <time.h>
#include <malloc.h>
#include "myfunction.h"
#define DELAY_ENABLED 0 /*Set this to 1 to enable including delay in hello message to server, otherwise it will be 0 by default*/
#define MAX_BUF_SIZE 64000
int main(int argc, char *argv[])
{
    struct sockaddr_in server_addr; /* Struct containing server address information*/
    int sfd; /* Server socket filed descriptor*/
    int cr; /* Connect result*/
    int i;
    
    /*Session information*/
    char measure_type[10];
    char n_probes[10];
    char msg_size[10];
    char server_delay[10] = "0";
    
    char hello_message[100] = "";
    char probe[MAX_BUF_SIZE] = "";
    clock_t time;
    double RTT;
    double mean_RTT;
    char token[10] = ""; /*Used in string manipulation algorithms*/
    char *payload; 
    int counter;
    char bye_message[5] = "b\n";
    
    short input_port;
    char input_ip[20];

    int stop;
    ssize_t byteRecv; /* Total number of bytes received*/
    ssize_t byteSent; /* Total number of bytes sent*/
    int sentNow; /* Number of bytes sent at a time*/
    int receivedNow; /* Number of bytes received at a time*/
    size_t msgLen;
    char receivedData [MAX_BUF_SIZE] = ""; /* Data to be received*/

    sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sfd < 0)
    {
        perror("socket"); /* Print error message*/
        exit(EXIT_FAILURE);
    }
	
    server_addr.sin_family = AF_INET;
    printf("Client --- Insert server port: ");
    scanf("%hd", &input_port);
    server_addr.sin_port = htons(input_port);
    printf("Client --- Insert server ip: ");
    scanf("%s", input_ip);
    server_addr.sin_addr.s_addr = inet_addr(input_ip);
    
    /*Config session - no check for wrong input*/
    printf("Client --- Insert measure type (rtt or thput): ");
    scanf("%s", measure_type);
	printf("Client --- Insert number of probes to be sent to server: ");
    scanf("%s", n_probes);
    printf("Client --- Insert message size: ");
    scanf("%s", msg_size);
    if(DELAY_ENABLED == 1)
    {
		printf("Client --- Insert server delay (seconds): ");
		scanf("%s", server_delay);
	}
    
    stop = atoi(msg_size);
    payload = (char*) malloc(stop+1/sizeof(char));
    for(i=0;i<stop;i++)
    {
		payload[i] = 'a';
	}
	payload[stop] = '\0';
    
    
	/*Connect to server*/
    cr = connect(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (cr< 0)
    {
        perror("connect"); /* Print error message*/
        exit(EXIT_FAILURE);
    }
    
    stop = 0;
    while(!stop)
    {
        /*Hello phase*/
        strcat(hello_message, "h");
        strcat(hello_message, " ");
        strcat(hello_message, measure_type);
		strcat(hello_message, " ");
		strcat(hello_message, n_probes);
		strcat(hello_message, " ");
		strcat(hello_message, msg_size);
		strcat(hello_message, " ");
		strcat(hello_message, server_delay);
		strcat(hello_message, "\n");
        
        msgLen = countStrLen(hello_message);
        byteSent = sendto(sfd, hello_message, msgLen, 0, (struct sockaddr *) &server_addr, sizeof(server_addr));
        printf("Client --- Hello message sent to server: %s\n", hello_message);
        
        byteRecv = recv(sfd, receivedData, MAX_BUF_SIZE, 0);
        printf("Client --- Ready message received from server\n");
        
        /*Measurement phase*/
        counter = 1;
        while(counter <= atoi(n_probes))
        {
			/*Building probe*/
			strcpy(probe, "m ");
			sprintf(token, "%d", counter);
			strcat(probe, token);
			strcat(probe, " ");
			strcat(probe, payload);
			strcat(probe, "\n\0");
			
			msgLen = countStrLen(probe);
			
			/*Start timer*/
			time = clock();
			
			/*Loop to prevent buffer limitations while sending data*/
			byteSent = 0;
			for(sentNow=0; byteSent != msgLen; byteSent += sentNow)
			{
				sentNow = sendto(sfd, probe + byteSent, msgLen - byteSent, 0, (struct sockaddr *) &server_addr, sizeof(server_addr));
				if (sentNow < 0)
				{
					perror("sendto");
					exit(EXIT_FAILURE);
				}
				printf("Client --- Sent %d bytes of probe %d\n", sentNow, counter);
			}

			printf("Client --- Probe %d sent to server\n", counter);
			
			/*Loop to prevent buffer limitiations while receiving data*/
			byteRecv = 0;
			for(receivedNow=0; byteRecv != msgLen; byteRecv += receivedNow)
			{
				receivedNow = recv(sfd, receivedData + byteRecv, msgLen - byteRecv, 0);
				if (receivedNow < 0)
				{
					perror("recv");
					exit(EXIT_FAILURE);
				}
				printf("Client --- Received back %d bytes of probe %d\n", receivedNow, counter);
			}
			/*Stop timer and add results*/	
			time = clock() - time;
			RTT = ((double)time)/CLOCKS_PER_SEC*1000;
			mean_RTT = mean_RTT + RTT;
			
			/*Add string terminator to echoed back probe*/
			for(i=0;;i++)
			{
				if(receivedData[i] == '\n')
				{
					break;
				}
			}
			receivedData[i+1] = '\0';
			
			/*Check for correctness of echoed back probe*/
			if(strcmp(receivedData, probe) != 0)
			{
				printf("Client --- Error, probe echoed back by server is not correct!\n");
				free(payload);
				close(sfd);
				return 0;
			}
			printf("Client --- Probe %d received back from server, RTT = %f ms\n\n", counter, RTT);
			
			counter++;
		} 
		
		/*Compute mean RTT*/
		mean_RTT = mean_RTT / atoi(n_probes);
		printf("Client --- Mean RTT is %f ms \n", mean_RTT);
		
		/*Compute throughput if required*/
		if(strcmp(measure_type, "thput") == 0)
		{
			mean_RTT = mean_RTT / 1000;
			mean_RTT = ((atoi(msg_size) + 5) / mean_RTT) * 8;
			printf("           THROUGHTPUT is %f bps \n", mean_RTT);
		}
		
		/*Bye phase*/
		msgLen = countStrLen(bye_message);
		byteSent = sendto(sfd, bye_message, msgLen, 0, (struct sockaddr *) &server_addr, sizeof(server_addr));
		printf("Client --- Bye message sent to server: %s", bye_message);
		
		byteRecv = recv(sfd, receivedData, MAX_BUF_SIZE, 0);
		printf("Client --- Bye confirmation received from server: %s\n", receivedData);
        
        stop=1;
    } /* End of while*/
    free(payload);
    close(sfd);
    return 0;
}

