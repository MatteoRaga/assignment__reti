#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <malloc.h>
#include "myfunction.h"
#define DELAY_ENABLED 0 /*Set this to 1 to enable server delay, otherwise there will be no artificial delay*/
#define N 3
#define MAX_BUF_SIZE 64000
#define SERVER_PORT 9876 /* Server port*/
#define BACK_LOG 2 /* Maximum queued requests*/
int main(int argc, char *argv[])
{
    struct sockaddr_in server_addr; /* Struct containing server address information*/
    struct sockaddr_in client_addr; /* Struct containing client address information*/
    int sfd; /* Server socket file descriptor*/
    int newsfd; /* Client communication socket - Accept result*/
    int br; /* Bind result*/
    int lr; /* Listen result*/
    char c_addr[INET_ADDRSTRLEN];

    int i;
    ssize_t byteRecv; /* Number of bytes received*/
    ssize_t byteSent; /* Number of bytes to be sent*/
    int sentNow; /* Number of bytes sent at a time*/
    int receivedNow; /* Number of bytes received at a time*/
    socklen_t cli_size;
    char receivedData [MAX_BUF_SIZE]; /* Data to be received*/
    
    /*Session information*/
    char measure_type[10];
    int n_probes;
    int msg_size;
    int server_delay;
    int counter;
    int effective_length;
    
    char hello_message[100];
    char probe_message[MAX_BUF_SIZE];
    char number[N];
    
    char *token;
    
    char ready_message[] = "200 OK - Ready";
    char closing_message[] = "200 OK - Closing";
    char hello_error[] = "404 ERROR - Invalid Hello message";
    char bye_error[] = "404 ERROR - Invalid Bye message, closing";
    char measurement_error[] = "404 ERROR - Invalid Measurement message";
    
    sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sfd < 0)
    {
        perror("socket"); /* Print error message*/
        exit(EXIT_FAILURE);
    }
    
    /* Initialize server address information*/
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    /* Convert to network byte order*/
    server_addr.sin_addr.s_addr = INADDR_ANY; /* Bind to any address*/

    br = bind(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (br < 0)
    {
        perror("bind"); /* Print error message*/
        exit(EXIT_FAILURE);
    } cli_size = sizeof(client_addr);
    
    /* Listen for incoming requests*/
    lr = listen(sfd, BACK_LOG);
    if (lr < 0)
    {
        perror("listen"); /* Print error message*/
        exit(EXIT_FAILURE);
    }
    
    for(;;){ /* Wait for incoming requests*/
        newsfd = accept(sfd, (struct sockaddr *) &client_addr, &cli_size);
        if (newsfd < 0)
        {
            perror("accept"); /* Print error message*/
            exit(EXIT_FAILURE);
        }
        
        /*Print client information*/
        printf("Server --- A client has connected\n");
        inet_ntop(AF_INET,&(client_addr.sin_addr),c_addr,INET_ADDRSTRLEN);
        if(c_addr == NULL)
        {
			perror("inet_ntop"); /* Print error message*/
			exit(EXIT_FAILURE);
		}
        printf("Server --- Client IP: %s\n", c_addr);
        printf("Server --- Client port: %d\n", htons(client_addr.sin_port));

        while(1)
        {
			/*Hello phase - waiting for hello message*/
            byteRecv = recv(newsfd, hello_message, sizeof(hello_message), 0);
            if (byteRecv < 0)
            {
                perror("recv");
                exit(EXIT_FAILURE);
            }
            printf("Server --- Received Hello Message\n");
            
            /*Processing hello message - checking correctness and getting session information*/
            token = strtok(hello_message, " ");
			if(strcmp(token, "h") != 0)
			{
				byteRecv = sizeof(hello_error);
				byteSent = send(newsfd, hello_error, byteRecv, 0);
				printf("Server --- Response sent back to client: %s", hello_error);
				close(newsfd);
				break;	
			}
			
			token = strtok(NULL, " ");
			if((strcmp(token,"rtt") != 0) && (strcmp(token,"thput") != 0))
			{
				byteRecv = sizeof(hello_error);
				byteSent = send(newsfd, hello_error, byteRecv, 0);
				printf("Server --- Response sent back to client: %s", hello_error);
				close(newsfd);
				break;
			}
			strcpy(measure_type, token);
			
			token = strtok(NULL, " ");
			n_probes = atoi(token);
			
			if(n_probes == 0)
			{
				byteRecv = sizeof(hello_error);
				byteSent = send(newsfd, hello_error, byteRecv, 0);
				printf("Server --- Response sent back to client: %s", hello_error);
				close(newsfd);
				break;
			}
			
			token = strtok(NULL, " ");
			msg_size = atoi(token);
			
			if(n_probes == 0)
			{
				byteRecv = sizeof(hello_error);
				byteSent = send(newsfd, hello_error, byteRecv, 0);
				printf("Server --- Response sent back to client: %s", hello_error);
				close(newsfd);
				break;
			}
			
			token = strtok(NULL, " ");
			server_delay = atoi(token);
			
			if((n_probes == 0) && (strcmp(token, "0") != 0))
			{
				byteRecv = sizeof(hello_error);
				byteSent = send(newsfd, hello_error, byteRecv, 0);
				printf("Server --- Response sent back to client: %s", hello_error);
				close(newsfd);
				break;
			}
           
			/*Ready for next phase - sending OK message*/
			byteRecv = sizeof(ready_message);
            byteSent = send(newsfd, ready_message, byteRecv, 0);
            if(byteSent != byteRecv)
            {
                perror("send");
                exit(EXIT_FAILURE);
            }
            printf("Server --- Response sent back to client: %s\n", ready_message);
            
            /*Measurement phase*/
            counter = 1;
            effective_length = msg_size + 5;
            number[0] = '\0';
            number[1] = '\0';
            number[2] = '\0';
            while(counter <= n_probes)
            {
				if(counter == 10) /*When probe number reach 10 (from 9) the length of the probe is expected to increment by 1*/
				{
					effective_length++;
				}
				
				/*Loop to get the whole message if buffer doesn't let doing a single transmission*/
				byteRecv = 0;
				for(receivedNow=0; byteRecv != effective_length; byteRecv += receivedNow)
				{
					receivedNow = recv(newsfd, probe_message + byteRecv, effective_length - byteRecv, 0);
					if (receivedNow < 0)
					{
						perror("recv");
						exit(EXIT_FAILURE);
					}
					printf("Server --- Received %d bytes of probe %d\n", receivedNow, counter);
				}
				
				printf("Server --- Probe %d successfully received!\n", counter);
				
				/*Add string terminator to received probe*/
				for(i=0;;i++)
				{
					if(probe_message[i] == '\n')
					{
						break;
					}
				}
				probe_message[i+1] = '\0';
				
				/*Uncomment this if you want each probe to be printed
				printf("Server --- %s\n", probe_message);
				*/
				
				/*Checking for correctness of probe*/
				if(probe_message[0] != 'm')
				{
					byteRecv = sizeof(measurement_error);
					byteSent = send(newsfd, measurement_error, byteRecv, 0);
					printf("Server1 --- Response sent back to client: %s", measurement_error);
					close(newsfd);
					break;	
				}
				
				i=0;
				while(probe_message[i+2] != ' ')
				{
					number[i] = probe_message[i+2];
					if(i==N)
					{
						byteRecv = sizeof(measurement_error);
						byteSent = send(newsfd, measurement_error, byteRecv, 0);
						printf("Server2 --- Response sent back to client: %s", measurement_error);
						close(newsfd);
						break;	
					}
					i++;
				}
				
				if(counter != atoi(number))
				{
					byteRecv = sizeof(measurement_error);
					byteSent = send(newsfd, measurement_error, byteRecv, 0);
					printf("Server3 --- Response sent back to client: %s", measurement_error);
					close(newsfd);
					break;	
				}
				
				if(DELAY_ENABLED == 1)
				{
					sleep(server_delay);
				}

				/*Echoing the probe back to client (same loop to prevent buffer problems)*/
				byteSent = 0;
				for(sentNow=0; byteSent != effective_length; byteSent += sentNow)
				{
					sentNow = sendto(newsfd, probe_message + byteSent, effective_length - byteSent, 0, (struct sockaddr *) &server_addr, sizeof(server_addr));
					if (sentNow < 0)
					{
						perror("sendto");
						exit(EXIT_FAILURE);
					}
					printf("Server --- %d bytes of probe %d sent back to client\n", sentNow, counter);
				}
				
				printf("Server --- Probe %d sent back to client\n", counter);
				
				counter++;
			}	
			
			/*Bye phase*/
			byteRecv = recv(newsfd, receivedData, sizeof(receivedData), 0);
            if (byteRecv < 0)
            {
                perror("recv");
                exit(EXIT_FAILURE);
            }
            printf("Server --- Received Bye message\n");
            
            if((receivedData[0] == 'b') && (receivedData[1] == '\n'))
            {
				byteRecv = sizeof(closing_message);
				byteSent = send(newsfd, closing_message, byteRecv, 0);
				printf("Server --- Response sent back to client: %s\n", closing_message);
			}else
			{
				byteRecv = sizeof(bye_error);
				byteSent = send(newsfd, bye_error, byteRecv, 0);
				printf("Server --- Response sent back to client: %s\n", bye_error);
			}
			close(newsfd);
            break;
        }
    } /* End of for(;;)*/
    close(sfd);
    return 0;
}

