#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "myfunction.h"
#define MAX_BUF_SIZE 64000
#define BACK_LOG 2 // Maximum queued requests

typedef struct helloMsg{
    char phase;
    char* measureType;
    int n_probes;
    int msg_size;
    int s_delay;
}t_helloMsg;

t_helloMsg hMsg;

char currentPhase = 'h';
int expetedProbe = 0;

/*function to check if the hello message is correct*/
int validateHelloMsg(char* receivedData){
    size_t msgLen = countStrLen(receivedData);
    printf("Hello message received from client ");
    printData(receivedData, msgLen);
    hMsg.phase = strtok(receivedData, " ")[0];
    hMsg.measureType = strtok(NULL, " ");
    hMsg.n_probes = atoi(strtok(NULL, " "));
    hMsg.msg_size = atoi(strtok(NULL, " "));
    hMsg.s_delay = atoi(strtok(NULL, "\n"));
    return hMsg.phase == currentPhase && (!strcmp(hMsg.measureType, "rtt") || !strcmp(hMsg.measureType, "thput")) && hMsg.n_probes > 0 ;
}
/*function to check if the probe is correct*/
int validateProbeMsg(char* receivedData){
    char msg[MAX_BUF_SIZE] = "m ";
    expetedProbe++;
    strcat(msg, stringConverter(expetedProbe));
    printf("verifico probe %d\n", expetedProbe);
    return strncmp(receivedData, msg, sizeof(msg)-1 );
}

int main(int argc, char *argv[]){
struct sockaddr_in server_addr; // struct containing server address information
struct sockaddr_in client_addr; // struct containing client address information
int sfd; // Server socket filed descriptor
int newsfd; // Client communication socket - Accept result
int br; // Bind result
int lr; // Listen result
int i;
int stop = 0;
int validMessage = 0;
ssize_t byteRecv; // Number of bytes received
ssize_t byteSent; // Number of bytes to be sent
ssize_t byteRicevuti = 0;
socklen_t cli_size;
char receivedData[MAX_BUF_SIZE]; // Data to be received
char* sendData = (char*) malloc(sizeof(char) * MAX_BUF_SIZE); // Data to be sent
char probe[MAX_BUF_SIZE];
size_t msgLen;
if(argc != 2){
    perror("required Server port number ");
    exit(EXIT_FAILURE);
}
sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
if (sfd < 0){
perror("socket"); // Print error message
exit(EXIT_FAILURE);
}

// Initialize server address information
server_addr.sin_family = AF_INET;
server_addr.sin_port = htons(atoi(argv[1])); // Convert to network byte order
server_addr.sin_addr.s_addr = INADDR_ANY; // Bind to any address
br = bind(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
if (br < 0){
    perror("bind"); // Print error message
    exit(EXIT_FAILURE);
}

cli_size = sizeof(client_addr);
// Listen for incoming requests
lr = listen(sfd, BACK_LOG);
if (lr < 0){
    perror("listen"); // Print error message
    exit(EXIT_FAILURE);
}
printf("TCP Server ready \n");

    for(;;){
        // Wait for incoming requests
        newsfd = accept(sfd, (struct sockaddr *) &client_addr, &cli_size);
        if (newsfd < 0){
        perror("accept"); // Print error message
        exit(EXIT_FAILURE);
        }

        /*Printf client address and port*/
        printf("client Address: %s Port: %d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

        while(1){
            strcpy(probe, "");
            strcpy(receivedData, "");
            byteRicevuti = 0;
            byteRecv = 0;
            do{
                byteRicevuti = recv(newsfd, probe, MAX_BUF_SIZE, 0);
                strcat(receivedData, probe);
                byteRecv += byteRicevuti;
                
            }while(currentPhase == 'm' && byteRecv < hMsg.msg_size);

            if (byteRecv < 0){
                perror("recv");
                exit(EXIT_FAILURE);
            }
            switch (currentPhase)
            {
            case 'h':
                validMessage = validateHelloMsg(receivedData);
                sendData = validMessage ? "200 OK - Ready \n" : "404 ERROR – Invalid Hello message \n";
                byteRecv = strlen(sendData);
                printf("String going to be send to client: %s", sendData);
                currentPhase = 'm';
                break;
            case 'm':
                validMessage = validateProbeMsg(receivedData);
                sendData = validMessage ? receivedData : "404 ERROR – Invalid Probe message \n";
                byteRecv = validMessage ? byteRecv : strlen("404 ERROR – Invalid Probe message \n");
                if(expetedProbe == hMsg.n_probes)
                    currentPhase = 'b';
                break;
            case 'b':
                printf("Bye message received from client ");
                printData(receivedData, byteRecv);
                strcpy(sendData, "200 OK - Closing \n");
                byteRecv = strlen("200 OK - Closing \n");
                printf("String going to be send to client: %s", sendData);
                stop = 1;
                break;
            }

            byteSent = send(newsfd, sendData, byteRecv, 0);

            if(!validMessage){
                close(newsfd);
                exit(EXIT_FAILURE);
            }
            if(stop){
                close(newsfd);
                exit(EXIT_SUCCESS);
            }
        }
    } 
    close(sfd);
    return 0;
}