#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "myfunction.h"
#include <sys/time.h>
#define MAX_BUF_SIZE 64000
#define MAX_CHAR 10

/*Function to calculate time in milliseconds*/
double currentTimeMillis() {
  struct timeval time;
  gettimeofday(&time, NULL);
  double s1 = (double)(time.tv_sec) * 1000000;
  double s2 = (double)((time.tv_usec + s1) / 1000);
  return s2;
}
    struct sockaddr_in server_addr; // struct containing server address information
    struct sockaddr_in client_addr; // struct containing client address information
    int sfd; // Server socket filed descriptor
    int br; // Bind result
    int cr; // Connect result
    int stop = 0;
    int n_probes;
    int msg_size;
    int s_delay = 0;
    char phase;
    char measure_type[MAX_CHAR];
    double msgRtt[20]; // rtt of each probe
    ssize_t byteRecv; // Number of bytes received
    ssize_t byteSent; // Number of bytes to be sent
    ssize_t byteRicevuti;
    size_t msgLen;
    socklen_t serv_size;
    char receivedData[MAX_BUF_SIZE]; // Data to be received
    char* sendData;
    char echo[MAX_BUF_SIZE];
    int i = 1;
    int count = 0;
    double t0, t1;

/*function to calculate mean Rtt*/
double meanRTT(double* val, int n_val){
    double avg = 0;
    for(int i = 0; i < n_val; i++){
        avg += val[i];
    }
    return avg/n_val;
}

void sendToServer(char* data){
    msgLen = countStrLen(data);
    byteSent = sendto(sfd, data, msgLen, 0, (struct sockaddr *)
    &server_addr, sizeof(server_addr));
}

void receiveFromServer(int is_echo){
    byteRicevuti = 0;
    strcpy(echo, " ");
    strcpy(receivedData, " ");
    do
    {
        byteRecv = recv(sfd, echo, MAX_BUF_SIZE, 0);
        byteRicevuti += byteRecv;
        strcat(receivedData, echo);

    } while (is_echo && byteRicevuti < byteSent);
}
/*function to insert parameters in messages
last is a flag to indicate if param is the last parameter*/
void insertParameter(char* param, int last){
    strcat(sendData, param);
    char* sp = last?"\n":" ";
    strcat(sendData, sp);
}
/*function to create the payload*/
void incrementPayload(char* param){
    strcat(sendData, param);
}
void ComposeHelloMsg(){
    insertParameter(&phase, 0);
    printf("Insert measure type: rtt for round trip time || thput for throughput: ");
    scanf("%s", measure_type);
    scanf("%*c");
    insertParameter(measure_type, 0);
    printf("Insert probe messages number: ");
    scanf("%d", &n_probes);
    insertParameter(stringConverter(n_probes), 0);
    printf("Insert probe messages size: ");
    scanf("%d", &msg_size);
    insertParameter(stringConverter(msg_size), 0);
    insertParameter(stringConverter(s_delay), 1);
}

void composeProbeMsg(int seq){
    int j;
    insertParameter("m", 0);
    insertParameter(stringConverter(seq), 0);
    for( j= 0; j <= (msg_size-1); j++){
        incrementPayload("p");
    }
    insertParameter("p", 1);
}

int main(int argc, char *argv[]){
    FILE *fd;
    sendData = (char*) malloc(sizeof(char) * MAX_BUF_SIZE); // Data to be sent 
    if(argc != 3){
        perror("required Server port number ");
        exit(EXIT_FAILURE);
    }
    sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sfd < 0){
        perror("socket"); // Print error message
        exit(EXIT_FAILURE);
    }
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(atoi(argv[2]));
    server_addr.sin_addr.s_addr = inet_addr(argv[1]);
    serv_size = sizeof(server_addr);
    cr = connect(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (cr< 0){
        perror("connect"); // Print error message
        exit(EXIT_FAILURE);
    }
        phase = 'h';
    while(!stop){
        switch(phase){
            case 'h':
                ComposeHelloMsg();
                printf("Hello Message going to be send to server: %s \n", sendData );
                sendToServer(sendData);
                receiveFromServer(0);
                printf("Received from server:");
                printData(receivedData, byteRecv);
                if(receivedData[1] == '4'){
                    stop = 1;
                    close(sfd);
                    exit(EXIT_FAILURE);
                }
                phase = 'm';
                printf("\n");
            break;
            case 'm':
                sprintf(sendData, "%s", "\0");
                composeProbeMsg(i);
                /*take the time before send the message*/
                t0 = currentTimeMillis();
                sendToServer(sendData);
                receiveFromServer(1);
                t1 = currentTimeMillis();
                /*Take the time after receiving echo*/
                /*calculate rtt of the probe*/
                msgRtt[count] = ((double)t1-t0);
                printf("Probe number %d RTT in ms: %f \n", count+1, msgRtt[count]);
                count++;
                i++;
                if(i > n_probes){
                    printf("\n");
                    double avg = meanRTT(msgRtt, count);
                    if(strcmp(measure_type, "rtt") == 0){
                        fd = fopen("rtt.txt", "a");
                        printf("RTT MEDIO in ms: %f \n", avg );
                        fprintf(fd, "%f %d \n", avg, msg_size);
                        fclose(fd);
                    }else{
                        fd = fopen("thput.txt", "a");
                        double thput = (byteSent * 0.008)/avg ;
                        printf("THPUT in Kbps: %.4f \n", thput );
                        fprintf(fd, "%f %ld \n", thput, byteSent);
                        fclose(fd);
                    }
                    phase = 'b';
                }
                printf("\n");
            break;
            case 'b':
                printf("Bye message sent to server \n");
                sendToServer("b\n");
                receiveFromServer(0);
                printf("Received from server:");
                printData(receivedData, byteRecv);
                printf("\n");
                if(strcmp(receivedData, "200 OK - Closing")){
                    close(sfd);
                    exit(EXIT_SUCCESS);
                }
            break;
            default:
            break;
        }
    }
    return 0;
}